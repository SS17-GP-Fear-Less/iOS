//
//  ViewController.swift
//  Patterns
//
//  Created by Macbook on 01/05/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class secondViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{

    @IBOutlet weak var collectionView: UICollectionView!
    
    var completePatternsArray = [patterns]()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupTableView()
        self.fillArrayFromJSON()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    func fillArrayFromJSON()
    {
        let namesPath = Bundle.main.path(forResource: "patterns", ofType: "txt")!
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: namesPath), options: .dataReadingMapped)
            let dic = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSDictionary
            
            completePatternsArray.removeAll()
            
            let _completeArray = dic.value(forKey: "allPatterns") as! NSArray
            
            for (_, _names) in _completeArray.enumerated() {
                
                let title = (_names as AnyObject).value(forKey: "title") as? String ?? ""
                let description = (_names as AnyObject).value(forKey: "description") as? String ?? ""
                let problem = (_names as AnyObject).value(forKey: "problem") as? String ?? ""
                let solution = (_names as AnyObject).value(forKey: "solution") as? String ?? ""

                completePatternsArray.append(patterns(title: title, description: description, problem: problem, solution: solution))
                
            }
            
            self.collectionView.reloadData()
        }
            
        catch {
            print("*** error while retreiving names ***")
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.completePatternsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "patternsCollectionCell", for: indexPath as IndexPath) as! patternsCollectionViewCell
        
        cell.patternTitle.text = completePatternsArray[indexPath.item].title
        
//        cell.backgroundColor = UIColor.brown.withAlphaComponent(0.2)
        
        //cell.borderWidth = CGFloat(self.selectedOnes[indexPath.item])
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        
//        self.numberCounter.text = "\(indexPath.row + 1)"
//        self.arabicWord.text = completeNamesArray[indexPath.item].urduName
//        self.englishName.text = completeNamesArray[indexPath.item].englishName
//        self.urduMeaning.text = completeNamesArray[indexPath.item].urduMeaning
//        self.englishMeaning.text = completeNamesArray[indexPath.item].englishMeaning
//        self.englishExplanation.text = completeNamesArray[indexPath.item].englishExplanation
        
        self.collectionView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
    
    func setupNavigationBar() {
        
        //self.navigationController?.navigationBar.barTintColor = UIColor.islam360_OrangeColor()
    }
    
    func setupTableView() {
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
}

class patterns {
    
    var title : String
    var description : String
    var problem : String
    var solution : String
    
    fileprivate init(title:String, description:String, problem : String, solution : String) {
        
        self.title = title
        self.description = description
        self.problem = problem
        self.solution = solution
    }
}

