//
//  SideMenuTableViewCell.swift
//  Quobay
//
//  Created by Amaan on 08/10/2016.
//  Copyright © 2016 Daniyal Raza. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {


    @IBOutlet var menuImage: UIImageView!
    @IBOutlet var menuTitle: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
