//
//  RegisterViewController.swift
//  Patterns
//
//  Created by Macbook on 28/09/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func RegisterButtonAction(_ sender: Any) {
        
        Patterns.sharedInstance.signInWithEmail(
            self.usernameTextField.text!,
            password: self.passwordTextField.text!,
            callback: {(error) in
                
                if(error == nil){
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let tabBarVC = storyBoard.instantiateViewController(withIdentifier: "RevealVC")
                    
                    self.present(tabBarVC, animated: true, completion:nil)
                }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
