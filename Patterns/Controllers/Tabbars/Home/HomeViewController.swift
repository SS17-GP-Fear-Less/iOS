//
//  PatternsViewController.swift
//  Patterns
//
//  Created by Macbook on 31/05/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class HomeViewController: CustomViewController {

    @IBOutlet weak var completePatternView: UIView!
    @IBOutlet weak var frontView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var problemText: UILabel!
    @IBOutlet weak var solutionText: UILabel!
    
    var isBackShown = false
    var selectedPattern : Patterns.PatternTab!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let patternsArrayReturned = Patterns.sharedInstance.getPatternsArrayFromUserDefaults(){
            
            Patterns.sharedInstance.patternsArray = patternsArrayReturned
        }
        else if(Patterns.sharedInstance.patternsArray.count == 0){
            self.callGetPatternsService()
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 2
        completePatternView.addGestureRecognizer(tap)
    }

    func callGetPatternsService()
    {
        self.disableScreen()
        
        Patterns.sharedInstance.getPatterns(
            
            { (error) in
                
                self.enableScreen()
                
                if(error == nil)
                {
                    Patterns.sharedInstance.savePatternsArrayInUserDefaults()
                    self.pickRandomCard()
                }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.topItem?.title = "Home"
        self.pickRandomCard()
    }
    
    func pickRandomCard(){
        if Patterns.sharedInstance.patternsArray.count > 0{
            
            let randomIndex = Int(arc4random_uniform(UInt32(Patterns.sharedInstance.patternsArray.count)))
            
            selectedPattern = Patterns.sharedInstance.patternsArray[randomIndex]
            self.fillValues()
        }
    }
    
    func fillValues(){
        titleLabel.text = selectedPattern.title
        descriptionLabel.text = selectedPattern.desc
        problemText.text = selectedPattern.problem
        solutionText.text = selectedPattern.solution
    }
    
    @objc func doubleTapped() {
        if isBackShown{
            UIView.transition(with: completePatternView, duration: 1.0, options: transitionOptions, animations: {
                
                self.backView.isHidden = true
                self.frontView.isHidden = false
            })
        }
        else{
            
            UIView.transition(with: completePatternView, duration: 1.0, options: transitionOptions, animations: {
                
                self.backView.isHidden = false
                self.frontView.isHidden = true
            })
        }
        
        isBackShown = !isBackShown
    }
}
