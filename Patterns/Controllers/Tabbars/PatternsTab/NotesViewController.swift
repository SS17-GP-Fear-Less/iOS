//
//  NotesViewController.swift
//  Patterns
//
//  Created by Macbook on 14/06/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class NotesViewController: CustomViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var notesDetailTitle: UILabel!
    @IBOutlet weak var notesDetailView: UIView!
    @IBOutlet weak var notesDetailTextView: UITextView!
    
    @IBOutlet weak var newNoteTitleTF: UITextField!
    @IBOutlet weak var newNoteTextView: UITextView!
    @IBOutlet weak var newNoteView: UIView!
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    var PatternObject : Patterns.PatternTab!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Notes"
        self.setupTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        self.blurScreen()
        newNoteView.isHidden = false
        newNoteTextView.text = ""
        newNoteTitleTF.text = ""
        self.view.bringSubview(toFront: self.newNoteView)
    }
    
    @IBAction func newNoteAddButtonAction(_ sender: Any) {
        
        let noteObject = Patterns.Notes(noteTitle: self.newNoteTitleTF.text!, noteText: self.newNoteTextView.text, noteDate: self.getCurrentDate())
        
        PatternObject.notesArray.append(noteObject)
        Patterns.sharedInstance.savePatternsArrayInUserDefaults()
        self.tableView.reloadData()
        
        self.returnPopUp()
    }
    
    @IBAction func newNoteCancelButtonAction(_ sender: Any) {
        self.returnPopUp()
    }
    
    func returnPopUp()
    {
        self.removeBlur()
        self.newNoteView.isHidden = true
        self.view.endEditing(true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return PatternObject.notesArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell") as! notesTableViewCell
        
        cell.fillData(notesObject: PatternObject.notesArray[indexPath.section])
 
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let thisView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 15))
        thisView.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
        return thisView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.blurScreen()
        self.notesDetailTextView.text = PatternObject.notesArray[indexPath.section].noteText
        self.notesDetailTitle.text = PatternObject.notesArray[indexPath.section].noteTitle
        self.notesDetailView.isHidden = false
        self.view.bringSubview(toFront: self.notesDetailView)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if(editingStyle == UITableViewCellEditingStyle.delete){
            self.PatternObject.notesArray.remove(at: indexPath.section)
            self.tableView.reloadData()
            Patterns.sharedInstance.savePatternsArrayInUserDefaults()
        }
    }

    func setupTableView() {
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 10
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView()
    }
    
    @IBAction func notesDetailCrossButton(_ sender: Any) {
        self.notesDetailView.isHidden = true
        self.removeBlur()
    }

}
