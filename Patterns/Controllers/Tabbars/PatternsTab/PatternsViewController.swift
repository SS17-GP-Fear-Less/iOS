//
//  HomeViewController.swift
//  Patterns
//
//  Created by Macbook on 31/05/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class PatternsViewController: CustomViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupCollectionView()
        if(Patterns.sharedInstance.patternsArray.count == 0){
            self.callGetPatternsService()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.topItem?.title = "Patterns"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ToPatternDetail", sender: Patterns.sharedInstance.patternsArray[indexPath.item])
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Patterns.sharedInstance.patternsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "patternCell", for: indexPath) as! PatternsCollectionViewCell
        
        cell.fillData(patternObject: Patterns.sharedInstance.patternsArray[indexPath.item])
        return cell
    }

    func setupCollectionView() {
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func callGetPatternsService()
    {        
        self.disableScreen()

        Patterns.sharedInstance.getPatterns(

            { (error) in

                self.enableScreen()

                if(error == nil)
                {
                    self.collectionView.reloadData()
                }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToPatternDetail"
        {
            let vc = segue.destination as! PatternDetailViewController
            vc.PatternObject = sender as! Patterns.PatternTab
        }
    }
}

