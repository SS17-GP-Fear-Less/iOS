//
//  HomeCollectionViewCell.swift
//  Patterns
//
//  Created by Macbook on 31/05/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class PatternsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var patternTitle: UILabel!
}


extension PatternsCollectionViewCell{
    
    func fillData(patternObject: Patterns.PatternTab){
        patternTitle.text = patternObject.title
    }
}
