//
//  tagsCollectionViewCell.swift
//  Patterns
//
//  Created by Macbook on 14/06/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class tagsCollectionViewCell: UICollectionViewCell {
    
    var onCrossButtonTapped : (() -> Void)? = nil
    @IBOutlet weak var tagTitle: UILabel!
    
    @IBAction func crossButtonAction(_ sender: Any) {
        if let onCrossButtonTapped = self.onCrossButtonTapped {
            onCrossButtonTapped()
        }
    }
}


extension tagsCollectionViewCell{
    
    func fillData(newTag: String){
        tagTitle.text = newTag
    }
}

