//
//  notesTableViewCell.swift
//  Patterns
//
//  Created by Macbook on 14/06/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class notesTableViewCell: UITableViewCell {

    @IBOutlet weak var noteTitle: UILabel!
    
    @IBOutlet weak var notesDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

extension notesTableViewCell{
    
    func fillData(notesObject: Patterns.Notes) {
        noteTitle.text = notesObject.noteTitle
        notesDate.text = notesObject.noteDate
    }
    
    func setProperties(cell:UITableViewCell)
    {
        cell.selectionStyle = UITableViewCellSelectionStyle.none
    }
}
