//
//  AddTagButtonCollectionViewCell.swift
//  Patterns
//
//  Created by Macbook on 27/08/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class AddTagButtonCollectionViewCell: UICollectionViewCell {
    
    var onAddTagButtonTapped : (() -> Void)? = nil
    
    @IBAction func AddTagButtonTapped(_ sender: Any) {
        if let onAddTagButtonTapped = self.onAddTagButtonTapped {
            onAddTagButtonTapped()
        }
    }
}

extension AddTagButtonCollectionViewCell{
    
}
