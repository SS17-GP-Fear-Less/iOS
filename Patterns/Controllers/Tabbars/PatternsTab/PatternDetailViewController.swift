//
//  PatternDetailViewController.swift
//  Patterns
//
//  Created by Macbook on 31/05/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class PatternDetailViewController: CustomViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var isBackShown = false
    var PatternObject : Patterns.PatternTab!
    
    @IBOutlet weak var notesButton: UIButton!
    @IBOutlet weak var tagsCollectionView: UICollectionView!
    @IBOutlet weak var newTagTextField: UITextField!
    @IBOutlet weak var AddTagView: UIView!
    @IBOutlet weak var PatternsDetailView: UIView!
    @IBOutlet weak var patternDetailBackView: UIView!
    @IBOutlet weak var patternDetailFrontView: UIView!
    
    @IBOutlet weak var patternImage: UIImageView!
    @IBOutlet weak var problemTextHeading: UILabel!
    @IBOutlet weak var problemText: UILabel!
    @IBOutlet weak var solutionTextHeading: UILabel!
    @IBOutlet weak var solutionText: UILabel!
    
    @IBOutlet weak var patternDesciption: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Pattern Insights"
        self.setupCollectionView()
        self.setUpViews()
    
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 2
        PatternsDetailView.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        notesButton.setImage(self.PatternObject.notesArray.count > 0 ? UIImage(named: "Notepad_Full") : UIImage(named: "Notepad_Empty") , for: .normal)
    }
    
    @objc func doubleTapped() {
        if isBackShown{
            UIView.transition(with: PatternsDetailView, duration: 1.0, options: transitionOptions, animations: {
                
                self.patternDetailBackView.isHidden = true
                self.patternDetailFrontView.isHidden = false
            })
        }
        else{
            
            UIView.transition(with: PatternsDetailView, duration: 1.0, options: transitionOptions, animations: {
                
                self.patternDetailBackView.isHidden = false
                self.patternDetailFrontView.isHidden = true
            })
        }
        
        isBackShown = !isBackShown
    }
    
    func setUpViews(){
        problemText.text = PatternObject.problem
        solutionText.text = PatternObject.solution
        patternDesciption.text = PatternObject.desc
        patternImage.image = UIImage(named: PatternObject.title)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
    }
    
    @IBAction func addButtonOnTagView(_ sender: Any) {
        if(newTagTextField.text != "")
        {
            PatternObject.tagsArray.append(newTagTextField.text!)
            self.tagsCollectionView.reloadData()
            
            self.returnPopUp()
        }
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.returnPopUp()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tagCell", for: indexPath) as! tagsCollectionViewCell
            
            cell.fillData(newTag: PatternObject.tagsArray[indexPath.item])
            cell.onCrossButtonTapped = {
                self.PatternObject.tagsArray.remove(at: indexPath.item)
                self.tagsCollectionView.reloadData()
            }
            
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddTagButtonCell", for: indexPath) as! AddTagButtonCollectionViewCell
            
            cell.onAddTagButtonTapped = {
                self.blurScreen()
                self.AddTagView.isHidden = false
                self.newTagTextField.text = ""
                self.view.bringSubview(toFront: self.AddTagView)
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var newSize = CGSize.zero
        
        if indexPath.section == 0{
            newSize.height = 35
            if (CGFloat(PatternObject.tagsArray[indexPath.item].characters.count) <= 3){
                newSize.width = CGFloat(PatternObject.tagsArray[indexPath.item].characters.count) * 20
            }
            else if (CGFloat(PatternObject.tagsArray[indexPath.item].characters.count) <= 9){
                newSize.width = CGFloat(PatternObject.tagsArray[indexPath.item].characters.count) * 14
            }
            else{
                newSize.width = CGFloat(PatternObject.tagsArray[indexPath.item].characters.count) * 11
            }
            
        }
        else{
            newSize.height = 35
            newSize.width = 110
        }
        return newSize
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return PatternObject.tagsArray.count
        }
        return 1
    }
    
    @IBAction func NotesButtonAction(_ sender: Any) {
        self.performSegue(withIdentifier: "toNotesVC", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toNotesVC"
        {
            let vc = segue.destination as! NotesViewController
            vc.PatternObject = self.PatternObject
        }
    }
    
    func setupCollectionView() {
        
        self.tagsCollectionView.delegate = self
        self.tagsCollectionView.dataSource = self
    }
    
    func returnPopUp()
    {
        self.removeBlur()
        self.AddTagView.isHidden = true
        self.view.endEditing(true)
    }
}
