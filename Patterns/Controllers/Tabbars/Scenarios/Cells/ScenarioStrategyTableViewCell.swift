//
//  ScenarioStrategyTableViewCell.swift
//  Patterns
//
//  Created by Macbook on 28/07/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class ScenarioStrategyTableViewCell: UITableViewCell {

    @IBOutlet weak var strategyTitle: UILabel!
    @IBOutlet weak var strategyDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

extension ScenarioStrategyTableViewCell{
    
    func fillData(strategyObject: Patterns.Strategies) {
        self.strategyTitle.text = strategyObject.title
        self.strategyDescription.text = strategyObject.desc
    }
    
}
