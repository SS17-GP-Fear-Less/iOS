//
//  EventsTableViewCell.swift
//  Patterns
//
//  Created by Macbook on 08/03/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class ScenariosTableViewCell: UITableViewCell {

    var onFavButtonTapped : (() -> Void)? = nil
    var onTickButtonTapped : (() -> Void)? = nil
    
    @IBAction func FavoriteButtonAction(_ sender: Any) {
        
        if let onButtonTapped = self.onFavButtonTapped {
            onButtonTapped()
        }
    }
    
    @IBAction func tickButtonAction(_ sender: Any) {
        
        if let onButtonTapped = self.onTickButtonTapped {
            onButtonTapped()
        }
    }
    
    @IBOutlet weak var scenarioView: UIView!
    @IBOutlet weak var scenarioTitle: UILabel!
    @IBOutlet weak var scenarioDescription: UILabel!
    @IBOutlet weak var numberOfLikes: UILabel!
    @IBOutlet weak var numberOfStrategies: UILabel!
    @IBOutlet weak var isLiked: UIButton!
    @IBOutlet weak var isFavourite: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension ScenariosTableViewCell{
    
    func fillData(scenarioObject: Patterns.Scenarios) {
        
        scenarioTitle.text = scenarioObject.title
        scenarioDescription.text = scenarioObject.desc
        numberOfStrategies.text = "\(scenarioObject.numberOfLikes)"
    }
    
    func setImages(scenarioObject: Patterns.Scenarios) {
        
        if scenarioObject.isFavourite {
            isFavourite.setBackgroundImage(UIImage(named: "heart_filled"), for: .normal)
        }
        else{
            isFavourite.setBackgroundImage(UIImage(named: "heart_empty"), for: .normal)
        }
    }
    
    func setProperties(cell:UITableViewCell)
    {
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        scenarioView.borderColor = UIColor.black
        scenarioView.borderWidth = 1.5
        scenarioView.layer.masksToBounds = true
    }
}



