//
//  StrategiesOfScenariosViewController.swift
//  Patterns
//
//  Created by Macbook on 28/07/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class StrategiesOfScenariosViewController: CustomViewController, UITableViewDelegate, UITableViewDataSource {
    
    var scenarioObject : Patterns.Scenarios!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupTableView()
        self.callGetAllStrategiesService()
    }

    func fillData() {
        
        scenarioObject.StrategiesArray = Patterns.sharedInstance.strategiesArray
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.topItem?.title = "Strategies"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return scenarioObject.StrategiesArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScenarioStrategyCell") as! ScenarioStrategyTableViewCell
        
        cell.fillData(strategyObject: scenarioObject.StrategiesArray[indexPath.section])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let thisView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 15))
        thisView.backgroundColor = UIColor.white
        return thisView
    }
    
    func setupTableView() {
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 10
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func callGetAllStrategiesService()
    {
        self.disableScreen()
        Patterns.sharedInstance.getAllStrategies(scenarioObject: scenarioObject,
            callback: { (error, resultStr) in
                
                self.enableScreen()
                
                if(error == nil)
                {
                    self.tableView.reloadData()
                }
        })
    }
}
