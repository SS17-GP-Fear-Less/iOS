//
//  ScenarioDetailsViewController.swift
//  Patterns
//
//  Created by Macbook on 28/07/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class ScenarioDetailsViewController: CustomViewController {

    var scenarioObject : Patterns.Scenarios!
    
    @IBOutlet weak var scenarioDetailFullView: UIView!
    @IBOutlet weak var scenarioTitle: UILabel!
    @IBOutlet weak var scenarioDescription: UITextView!
    @IBOutlet weak var numberOfStrategies: UILabel!
    @IBOutlet weak var showCommentsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.fillValues()
        self.callGetAllStrategiesFoScenarioService()
    }

    func fillValues()
    {
        self.scenarioTitle.text = scenarioObject.title
        self.scenarioDescription.text = scenarioObject.desc
        self.numberOfStrategies.text = "Strategies: \(scenarioObject.numberOfStrategies)"
    }
    
    @IBAction func showCommentsButtonAction(_ sender: Any) {
    
    }
    
    @IBAction func goToStrategiesButtonAction(_ sender: Any) {
        
        self.performSegue(withIdentifier: "toStrategiesFromScenarioVC", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toStrategiesFromScenarioVC"{
            let vc = segue.destination as! StrategiesOfScenariosViewController
            vc.scenarioObject = scenarioObject
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func callGetAllStrategiesFoScenarioService()
    {
        self.disableScreen()
        Patterns.sharedInstance.getAllStrategiesForScenario(scenarioObject: self.scenarioObject, URLOfStrategy: self.scenarioObject.strategiesArrayLink,
            callback: { (error, resultStr) in
                
                self.enableScreen()
                
                if(error == nil)
                {
                    self.numberOfStrategies.text = "Strategies: \(self.scenarioObject.StrategiesArray.count)"
                }
        })
    }

}
