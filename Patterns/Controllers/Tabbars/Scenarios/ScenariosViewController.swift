//
//  FirstViewController.swift
//  Patterns
//
//  Created by Macbook on 02/03/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class ScenariosViewController: CustomViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var newScenarioCreateButtionAction: UIButton!
    @IBOutlet weak var newScenarioDescription: UITextView!
    @IBOutlet weak var newScenarioTitle: UITextField!
    @IBOutlet weak var createNewView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBAction func unwindToMenu(segue: UIStoryboardSegue) {}

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        
        if let challengesArrayReturned = Patterns.sharedInstance.getChallengesArrayFromUserDefaults(){
            Patterns.sharedInstance.scenariosArray = challengesArrayReturned
            self.tableView.reloadData()
        }
        else{
            self.callGetAllScenariosService()
        }
    }
    
    @IBAction func segmentControlAction(_ sender: Any) {
        if(segmentControl.selectedSegmentIndex == 0){
            self.tableView.isHidden = false
            self.createNewView.isHidden = true
            self.tableView.reloadData()
        } else {
            self.tableView.isHidden = true
            self.createNewView.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.topItem?.title = "Challenges"
    }
    
    @IBAction func createNewScenarioButtonAction(_ sender: Any) {
        self.callAddScenarioService()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(true)
    }
    
    @objc func addTapped()
    {
        self.performSegue(withIdentifier: "toCheckInScreen", sender: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.items![0].rightBarButtonItem = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Patterns.sharedInstance.scenariosArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScenarioCell") as! ScenariosTableViewCell
        
        cell.setProperties(cell: cell)
        cell.fillData(scenarioObject: Patterns.sharedInstance.scenariosArray[indexPath.section])
        cell.setImages(scenarioObject: Patterns.sharedInstance.scenariosArray[indexPath.section])
        
        cell.onFavButtonTapped = {
            Patterns.sharedInstance.scenariosArray[indexPath.section].isFavourite = !Patterns.sharedInstance.scenariosArray[indexPath.section].isFavourite
            
            Patterns.sharedInstance.saveChallengesArrayInUserDefaults()
            
            cell.setImages(scenarioObject: Patterns.sharedInstance.scenariosArray[indexPath.section])
        }
        
        cell.onTickButtonTapped = {
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toScenarioDetailVC", sender: Patterns.sharedInstance.scenariosArray[indexPath.section])
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let thisView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 15))
        thisView.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1.0)
        return thisView
    }
    
    func setupTableView() {
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 10
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toScenarioDetailVC"{
            let vc = segue.destination as! ScenarioDetailsViewController
            vc.scenarioObject = sender as! Patterns.Scenarios
        }
    }
    
    func callAddScenarioService()
    {
        self.disableScreen()
        Patterns.sharedInstance.addScenario(title: newScenarioTitle.text!, description: newScenarioDescription.text,

            callback: { (error, resultStr) in

                self.enableScreen()

                if(error == nil)
                {
                    self.newScenarioTitle.text = ""
                    self.newScenarioDescription.text = ""
                    Patterns.sharedInstance.saveChallengesArrayInUserDefaults()
                    Utilities.showAlertWithTitle("", message: "New Challenge Succesfully Added.", viewController: self, okButtonCallback: {}, alertDidShow: {})
                }
        })
    }
    
    func callGetAllScenariosService()
    {
        self.disableScreen()
        Patterns.sharedInstance.getAllScenarios(
            
            callback: { (error, resultStr) in
                
                self.enableScreen()
                
                if(error == nil)
                {
                    Patterns.sharedInstance.saveChallengesArrayInUserDefaults()
                    self.tableView.reloadData()
                }
        })
    }
}
