//
//  FirstViewController.swift
//  Patterns
//
//  Created by Macbook on 02/03/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class StrategiesViewController: CustomViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIPopoverPresentationControllerDelegate, AddPatternInStrategyDelegate {
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var newStrategyCreateButton: UIButton!
    @IBOutlet weak var newStrategyDescription: UITextView!
    @IBOutlet weak var newStrategyTitle: UITextField!
    @IBOutlet weak var newStrategyView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBAction func unwindToMenu(segue: UIStoryboardSegue) {}
    @IBOutlet weak var patternStrategyCollectionView: UICollectionView!
    
    @IBOutlet weak var selectScenarioButton: UIButton!
    @IBOutlet weak var selectedScenatioTitle: UILabel!
    var isCallingSelectScenarioScreen = false
    
    
    var selectedChallengeObject : Patterns.Scenarios!
    var selectedPatternsArray = [String]()
    var newStrategyObject : Patterns.Strategies!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        self.setupCollectionView()
        
        newStrategyObject = Patterns.Strategies(strategyID: "01", title: "", username: "", desc: "", isFavourite: false, challengeLink: "",  patternsURL: "")
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        if let strategiesArrayReturned = Patterns.sharedInstance.getStrategiesArrayFromUserDefaults(){
            Patterns.sharedInstance.strategiesArray = strategiesArrayReturned
            self.tableView.reloadData()
        }
        else{
            self.callGetAllStrategiesService()
        }
        
        self.navigationController?.navigationBar.topItem?.title = "Strategies"
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.items![0].rightBarButtonItem = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func returnPopOverForPattern(patternObject: Patterns.PatternTab) {
        newStrategyObject.patternsArray.append(patternObject)        
        patternStrategyCollectionView.reloadData()
    }
    
    func returnPopOverForScenario(scenarioObject: Patterns.Scenarios) {
        selectedChallengeObject = scenarioObject
        selectedScenatioTitle.text = selectedChallengeObject.title
    }
    
    func setupCollectionView() {
        
        self.patternStrategyCollectionView.delegate = self
        self.patternStrategyCollectionView.dataSource = self
    }
    
    func setupTableView() {
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 10
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }

    
    func popoverControllerDidDismissPopover(popoverController: UIModalPresentationStyle) {
        self.removeBlur()
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
                                // Collection View start //
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController)
    {}

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var newSize = CGSize.zero
        
        if indexPath.section == 0{
            newSize.height = 35
            newSize.width = CGFloat(newStrategyObject.patternsArray[indexPath.item].title.characters.count) * 11
        }
        else{
            newSize.height = 35
            newSize.width = 110
        }
        return newSize
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return newStrategyObject.patternsArray.count
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "patternForStrategyCell", for: indexPath) as! PatternForStrategyCollectionViewCell
            
            cell.fillData(newPattern: newStrategyObject.patternsArray[indexPath.item])
            cell.onCrossButtonTapped = {
            }
            
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "patternForAddStrategyCell", for: indexPath) as! patternForAddStrategyCollectionViewCell
            
            cell.onAddStrategyButtonTapped = {
                self.isCallingSelectScenarioScreen = false
                self.performSegue(withIdentifier: "toAddPatternPopup", sender: self)
            }
            return cell
        }
    }
                                    // Collection View End //
    
    @IBAction func segmentControlAction(_ sender: Any) {
        if(segmentControl.selectedSegmentIndex == 0){
            self.tableView.isHidden = false
            self.newStrategyView.isHidden = true
            
        } else {
            self.tableView.isHidden = true
            self.newStrategyView.isHidden = false
        }
    }
    
    
    @IBAction func createNewStrategyButtonAction(_ sender: Any) {
        
        self.callAddStrategyService()
    }
    
    
    @objc func addTapped(){
        self.performSegue(withIdentifier: "toCheckInScreen", sender: self)
    }
    
    @IBAction func selectScenarioButtonAction(_ sender: Any) {
        self.isCallingSelectScenarioScreen = true
        self.performSegue(withIdentifier: "toAddPatternPopup", sender: self)
    }
    
                                    // Table View start //
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Patterns.sharedInstance.strategiesArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StrategyCell") as! StrategiesTableViewCell
        
        cell.setProperties(cell: cell)
        cell.fillData(strategyObject: Patterns.sharedInstance.strategiesArray[indexPath.section])
        cell.setFavImage(strategyObject: Patterns.sharedInstance.strategiesArray[indexPath.section])
        
        cell.onFavButtonTapped = {
            Patterns.sharedInstance.strategiesArray[indexPath.section].isFavourite = !Patterns.sharedInstance.strategiesArray[indexPath.section].isFavourite
            cell.setFavImage(strategyObject: Patterns.sharedInstance.strategiesArray[indexPath.section])
            
            Patterns.sharedInstance.saveStrategiesArrayInUserDefaults()
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toStrategyDetailVC", sender: Patterns.sharedInstance.strategiesArray[indexPath.section])
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let thisView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 15))
        thisView.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1.0)
        return thisView
    }
                                        // Table View End //
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toStrategyDetailVC"{
            let vc = segue.destination as! StrategyDetailViewController
            vc.strategyObject = sender as! Patterns.Strategies
        }
        else if segue.identifier == "toAddPatternPopup"
        {
            let popOver = segue.destination as! AddPatternInStrategyViewController
            popOver.AddPatternDelegate = self
            popOver.showScenarioDetails = isCallingSelectScenarioScreen
            popOver.modalPresentationStyle = .popover
            popOver.preferredContentSize = CGSize(width: self.view.frame.width - 100, height: 300)
            
            let controller = popOver.popoverPresentationController
            if controller != nil{
                controller?.delegate = self
                controller?.permittedArrowDirections = UIPopoverArrowDirection()
                controller?.sourceRect = self.view.frame
                controller?.sourceView = self.view
                controller?.sourceRect.origin = self.view.frame.origin
            }
        }
    }
    
    func callAddStrategyService()
    {
        for obj in newStrategyObject.patternsArray{
            selectedPatternsArray.append(obj.patternID)
        }
        
        self.disableScreen()
        
        Patterns.sharedInstance.addStrategy(
            challenge: selectedChallengeObject.scenarioId,
            title: newStrategyTitle.text!, description: newStrategyDescription.text,
            patterns: selectedPatternsArray,
            callback: { (error, resultStr) in
                
                self.enableScreen()
                
                if(error == nil)
                {
                    Patterns.sharedInstance.saveStrategiesArrayInUserDefaults()
                    self.emptyAllTheFields()
                    Utilities.showAlertWithTitle("", message: "Strategy Succesfully added.", viewController: self, okButtonCallback: {}, alertDidShow: {})
                }
                })
    }
    
    func emptyAllTheFields(){
        self.newStrategyTitle.text = ""
        self.newStrategyDescription.text = ""
        self.selectedScenatioTitle.text = ""
        self.newStrategyObject.patternsArray.removeAll()
        self.patternStrategyCollectionView.reloadData()
    }
    
    func callGetAllStrategiesService()
    {
        self.disableScreen()
        Patterns.sharedInstance.getAllStrategies(
            scenarioObject: nil,
            callback: { (error, resultStr) in
                self.enableScreen()
                
                if(error == nil)
                {
                    Patterns.sharedInstance.saveStrategiesArrayInUserDefaults()
                    self.tableView.reloadData()
                }
        })
    }
}
