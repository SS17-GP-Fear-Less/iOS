//
//  PatternForStrategyCollectionViewCell.swift
//  Patterns
//
//  Created by Macbook on 25/08/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class PatternForStrategyCollectionViewCell: UICollectionViewCell {
    
    var onCrossButtonTapped : (() -> Void)? = nil
    @IBOutlet weak var patternTitle: UILabel!
    
    @IBAction func crossButtonAction(_ sender: Any) {
        if let onCrossButtonTapped = self.onCrossButtonTapped {
            onCrossButtonTapped()
        }
    }
}

extension PatternForStrategyCollectionViewCell{
    
    func fillData(newPattern: Patterns.PatternTab){
        patternTitle.text = newPattern.title
    }
}
