//
//  AddPatternTableViewCell.swift
//  Patterns
//
//  Created by Macbook on 26/08/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class AddPatternTableViewCell: UITableViewCell {
    
    @IBOutlet weak var patternTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
    }
}

extension AddPatternTableViewCell{
    
    func fillData(title: String) {
        patternTitle.text = title
    }
}
