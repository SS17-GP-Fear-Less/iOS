//
//  EventsTableViewCell.swift
//  Patterns
//
//  Created by Macbook on 08/03/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class StrategiesTableViewCell: UITableViewCell {

    var onFavButtonTapped : (() -> Void)? = nil
    
    @IBAction func favoriteButtonAction(_ sender: Any) {
        
        if let onButtonTapped = self.onFavButtonTapped {
            onButtonTapped()
        }
    }
    
    @IBOutlet weak var strategyTitle: UILabel!
    @IBOutlet weak var strategyUsername: UILabel!
    @IBOutlet weak var StrategyText: UILabel!
    @IBOutlet weak var isFavourite: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension StrategiesTableViewCell{
    
    func fillData(strategyObject: Patterns.Strategies) {
        
        strategyTitle.text = strategyObject.title
        //strategyUsername.text = strategyObject.username
        StrategyText.text = strategyObject.desc
    }
    
    func setFavImage(strategyObject: Patterns.Strategies) {
        
        if strategyObject.isFavourite {
            isFavourite.setBackgroundImage(UIImage(named: "heart_filled"), for: .normal)
        }
        else{
            isFavourite.setBackgroundImage(UIImage(named: "heart_empty"), for: .normal)
        }
    }
    
    func setProperties(cell:UITableViewCell)
    {
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.layer.cornerRadius = 3
        cell.layer.masksToBounds = true
        cell.borderColor = UIColor.black.withAlphaComponent(0.08)
        cell.borderWidth = 1.5
    }
}



