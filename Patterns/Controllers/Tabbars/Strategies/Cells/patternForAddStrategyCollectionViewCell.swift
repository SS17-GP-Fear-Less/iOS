//
//  patternForAddStrategyCollectionViewCell.swift
//  Patterns
//
//  Created by Macbook on 26/08/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class patternForAddStrategyCollectionViewCell: UICollectionViewCell {
    
    var onAddStrategyButtonTapped : (() -> Void)? = nil
    
    @IBAction func AddStrategyButtonTapped(_ sender: Any) {
        if let onAddStrategyButtonTapped = self.onAddStrategyButtonTapped {
            onAddStrategyButtonTapped()
        }
    }
}

extension patternForAddStrategyCollectionViewCell{
    
}
