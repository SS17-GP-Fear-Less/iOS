//
//  StrategyDetailViewController.swift
//  Patterns
//
//  Created by Macbook on 28/07/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class StrategyDetailViewController: CustomViewController {
    
    var strategyObject : Patterns.Strategies!

    @IBOutlet weak var strategyDetailFullView: UIView!
    @IBOutlet weak var strategyTitle: UILabel!
    @IBOutlet weak var strategyText: UILabel!
    @IBOutlet weak var strategyDescription: UITextView!
    @IBOutlet weak var ChallengeText: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.callGetAllStrategiesService()
        self.callGetChallengeService()
        self.fillValues()
    }

    func fillValues()
    {
        for obj in strategyObject.patternsArray{
            self.strategyText.text = self.strategyText.text! + " -> " + obj.title
        }
        
        self.strategyTitle.text = strategyObject.title
        self.strategyDescription.text = strategyObject.desc
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func callGetAllStrategiesService()
    {
        self.disableScreen()
        Patterns.sharedInstance.getAllPatternsForStrategy(
            strategyObject: self.strategyObject,
            callback: { (error, resultStr) in
                
                self.enableScreen()
                if(error == nil)
                {
                    for obj in self.strategyObject.patternsArray{
                        self.strategyText.text = (self.strategyText.text == "" ? obj.title : self.strategyText.text! + " -> " + obj.title)
                    }
                }
        })
    }
    
    func callGetChallengeService()
    {
        self.disableScreen()
        
        Patterns.sharedInstance.getAScenarioForSelectedStrategy(
            strategyObject: self.strategyObject,
            callback: { (error, resultStr) in
                
                self.enableScreen()
                if(error == nil)
                {
                    self.ChallengeText.text =  self.strategyObject.challengeObject.title
                }
        })
    }
}
