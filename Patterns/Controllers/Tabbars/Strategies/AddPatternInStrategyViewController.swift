//
//  AddPatternInStrategyViewController.swift
//  Patterns
//
//  Created by Macbook on 26/08/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class AddPatternInStrategyViewController: CustomViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var AddPatternDelegate :AddPatternInStrategyDelegate?
    var showScenarioDetails = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if showScenarioDetails{
            return Patterns.sharedInstance.scenariosArray.count
        }
        return Patterns.sharedInstance.patternsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddPatternCell") as! AddPatternTableViewCell
        
        if showScenarioDetails{
            cell.fillData(title: Patterns.sharedInstance.scenariosArray[indexPath.row].title)
        }
        else{
            cell.fillData(title: Patterns.sharedInstance.patternsArray[indexPath.row].title)
        }
        
        
        return cell
    }
    
    @IBAction func cancelButtonAction(sender: AnyObject)
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func AddButtonAction(_ sender: Any) {
        
        if tableView.indexPathForSelectedRow?.row != nil{
            
            if showScenarioDetails{
                AddPatternDelegate?.returnPopOverForScenario(scenarioObject: Patterns.sharedInstance.scenariosArray[(tableView.indexPathForSelectedRow?.row)!])
            }else{
                AddPatternDelegate?.returnPopOverForPattern(patternObject: Patterns.sharedInstance.patternsArray[(tableView.indexPathForSelectedRow?.row)!])
                
            }
            
            dismiss(animated: true, completion: nil)
        }
        else{
            Utilities.showAlertWithTitle("", message: "Please select a pattern first", viewController: self, okButtonCallback: {}, alertDidShow:{})
        }
    }

    func setupTableView() {
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 10
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
}

protocol AddPatternInStrategyDelegate
{
    func returnPopOverForPattern(patternObject: Patterns.PatternTab)
    func returnPopOverForScenario(scenarioObject: Patterns.Scenarios)
}
