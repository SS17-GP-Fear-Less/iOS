//
//  Utilities.swift
//  Hit With Me
//
//  Created by Plutotek on 20/11/2016.
//  Copyright © 2016 Plutotek. All rights reserved.
//

import Foundation
import UIKit


class  Utilities {
    
    
    class func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        print(emailTest.evaluate(with: testStr))
        
        return emailTest.evaluate(with: testStr)
        
    }
    
    
    class func showAlertWithTitle(_ title:String?, message:String?, viewController:UIViewController, okButtonCallback:@escaping ()-> Void, alertDidShow:@escaping ()->Void) {
        
        
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        controller.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            
            okButtonCallback()
            
        }))
        
        viewController.present(controller, animated: true) {
            
            alertDidShow()
        }
        
        
    }
    
    class func logOutFunction(_ title:String?, message:String?, viewController:UIViewController, okButtonCallback:@escaping ()-> Void, alertDidShow:@escaping ()->Void) {
        
        
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        controller.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (action) in
            
            okButtonCallback()
        }))
        
        
        controller.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil) )
        
        
        
        viewController.present(controller, animated: true) {
            
            alertDidShow()
        }
    }
    
    
    class func printResponse(_ value:AnyObject) {
        
        print(value)
        
    }
    
    class func getDateFromDateTime(_ date:String) -> String
    {
        var myStringArr = date.components(separatedBy: "T")
        if myStringArr.count > 0
        {
            return myStringArr[0]
        }
        
        return date
    }
    
    class func generateBarcode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CICode128BarcodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }

    
}
