//
//  ContainerViewController.swift
//  Atlas
//
//  Created by zain on 21/07/2016.
//  Copyright © 2016 test. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {

    var segueIdentifier:String!
    var lastViewController:UIViewController!
    var Vc:UIViewController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Patterns.sharedInstance.presentViewSegue != nil
        {
            self.segueIdentifier = Patterns.sharedInstance.presentViewSegue
            performSegue(withIdentifier: self.segueIdentifier, sender: nil)
        }
        else
        {
            self.segueIdentifier = "ToTabBarsVC"
            performSegue(withIdentifier: self.segueIdentifier, sender: nil)
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == self.segueIdentifier
        {
            if lastViewController != nil
            {
                lastViewController.view.removeFromSuperview()
            }
            
            Vc = segue.destination
            self.addChildViewController(Vc)
            self.view.addSubview(Vc.view)
            lastViewController = Vc
            
        }
    }
}
