//
//  SideMenuViewController.swift
//  Quobay
//
//  Created by Daniyal Raza on 8/9/16.
//  Copyright © 2016 Daniyal Raza. All rights reserved.
//

import UIKit

class SideMenuViewController: CustomViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    var selectedIndex:Int?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.setUpView()
    }

    override func viewDidLayoutSubviews()
    {
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
    }
    
    func setUpView()
    {
        //username.text = Patterns.sharedInstance.appUser.name
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Patterns.sharedInstance.sidebarArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SlideMenuCell") as! SideMenuTableViewCell
        
        cell.menuTitle?.text = Patterns.sharedInstance.sidebarArray[indexPath.row][0]
        cell.menuImage.image = UIImage(named: Patterns.sharedInstance.sidebarArray[indexPath.row][1])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let title = Patterns.sharedInstance.sidebarArray[indexPath.row][0]
        if(indexPath.row == Patterns.sharedInstance.sidebarArray.count - 1)
        {
            Utilities.logOutFunction("Alert", message: "Are you sure you want to logout?", viewController: self, okButtonCallback: {
                
                Patterns.sharedInstance.logout()
                self.jumpToLoginScreen()
                
            }, alertDidShow: {
            })
        }
        
        else
        {
            Patterns.sharedInstance.presentViewSegue = Patterns.sharedInstance.sidebarArray[indexPath.row][2]
            performSegue(withIdentifier: "toNavigationController", sender: nil)
        }
    }
    
    func sendToRespectiveScreen(indexPath: IndexPath)
    {
        Patterns.sharedInstance.presentViewSegue = Patterns.sharedInstance.sidebarArray[indexPath.row][2]
        Patterns.sharedInstance.websiteLink = Patterns.sharedInstance.sidebarArray[indexPath.row][3]
        
        performSegue(withIdentifier: "toNavigationController", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let ratio = self.view.frame.size.width / 320
        return ratio * 50
    }
    
    func jumpToLoginScreen(){
        let storyBoard = UIStoryboard(name: "Authentication", bundle: nil)
        let tabBarVC = storyBoard.instantiateViewController(withIdentifier: "AuthNC")
        
        self.present(tabBarVC, animated: true, completion:nil)
    }
    
    func jumpToInstruction() {
        self.performSegue(withIdentifier: "ToWalkthroughVC", sender: nil)
    }
    
    func callSidebarItems()
    {
        self.disableScreen()
        
//        Patterns.sharedInstance.getSidebarItems(
//            
//            { (error, resultStr) in
//                
//                self.enableScreen()
//                
//                if(error == nil)
//                {
//                    self.tableView.reloadData()
//                }
//        })
    }
}
