//
//  CustomViewController.swift
//  Hit With Me
//
//  Created by Plutotek on 31/10/2016.
//  Copyright © 2016 Plutotek. All rights reserved.
//

import UIKit
import Foundation
import MapKit

class CustomViewController: UIViewController{
    
    var blurView = UIView()
    var indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    let transitionOptions: UIViewAnimationOptions = [.transitionFlipFromRight, .showHideTransitionViews]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        blurView.backgroundColor = UIColor.init(red: 150/255.0, green: 150/255.0, blue: 150/255.0, alpha: 0.5)
        
        setupIndicator()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(CustomViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CustomViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }

    func setupIndicator()
    {
        self.indicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        self.indicator.backgroundColor = UIColor.init(red: 200/255.0, green: 200/255.0, blue: 200/255.0, alpha: 1)
        self.indicator.color = UIColor.white
        self.indicator.center = self.view.center
    }
    
    func getCurrentDate() -> String {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let result = formatter.string(from: date)
        
        return result
    }
    
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    // Screen height.
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //IQKeyboardManager.sharedManager().enable = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        //IQKeyboardManager.sharedManager().enable = true
    }
    
    func disableScreen() {
        
        self.blurView.frame = self.view.bounds
        self.indicator.center = self.view.center
        
        self.view.addSubview(self.blurView)
        self.view.addSubview(self.indicator)
        
        self.indicator.startAnimating()
        self.blurView.isHidden = false
    }
    
    func blurScreen() {
        
        self.blurView.frame = self.view.bounds
        self.view.addSubview(self.blurView)
        self.blurView.isHidden = false
    }
    
    func removeBlur() {
        
        self.blurView.isHidden = true
        self.blurView.removeFromSuperview()
    }
    
    func enableScreen() {
        
        self.blurView.isHidden = true
        self.blurView.removeFromSuperview()
        
        self.indicator.stopAnimating()
        self.indicator.removeFromSuperview()
    }
    
    // MARK:- Keyboard Functions
    
    @objc func keyboardWillHide(_ notification:Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            self.view.frame.origin.y = 0
        })
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as AnyObject).cgRectValue.size

        UIView.animate(withDuration: 0.2, animations: {
            self.view.frame.origin.y = -keyboardSize.height/3
        })
    }
}
