//
//  Patterns.swift
//  Hit With Me
//
//  Created by Plutotek on 20/11/2016.
//  Copyright © 2016 Plutotek. All rights reserved.
//

import Foundation
import Alamofire
import MapKit
//import SDWebImage
//import JSQMessagesViewController

class Patterns {
    
    class var sharedInstance : Patterns {
        
        struct Singleton {
            static var instance = Patterns()
        }
        return Singleton.instance
    }
    
    var thankyouString : String!
    
    fileprivate(set) var appUser : AppUser!
    
    var presentViewSegue:String?
    var presentView:String?
    var navigationBarTitle:String?
    var websiteLink:String?
    
    var scenariosArray = [Scenarios]()
    var strategiesArray = [Strategies]()
    var patternsArray = [PatternTab]()

    var sidebarArray = [["",""]]
    
    //zain
    func getAllScenarios(callback:@escaping (_ error:String?, _ resultStr:String?)->Void) {
        
        Alamofire.request(Constants.baseURL + Constants.getAllScenariosURL, method: .get,  parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            let value = response.result.value as AnyObject
            let embeddedObject = value.value(forKey:"_embedded") as AnyObject
            let strategyObject = embeddedObject.value(forKey:"challenges") as AnyObject
            if let valuee = strategyObject as? NSArray {
                
                self.scenariosArray.removeAll()
                
                for i in 0..<valuee.count
                {
                    if let completeMessage = valuee[i] as? NSDictionary
                    {
                        let scenarioObject = Scenarios(
                            scenarioId: ((completeMessage.value(forKey: "_links") as? AnyObject)?.value(forKey: "challenge") as? AnyObject)?.value(forKey: "href") as? String ?? "",
                         title: completeMessage.value(forKey: "title") as? String ?? "",
                         desc: completeMessage.value(forKey: "description") as? String ?? "",
                         isFavourite: false,
                         numberOfStrategies: String(completeMessage.value(forKey: "strategies") as? Int ?? 0),
                         numberOfLikes: completeMessage.value(forKey: "thumbsUp") as? Int ?? 0,
                         strategiesArrayLink: ((completeMessage.value(forKey: "_links") as? AnyObject)?.value(forKey: "strategies") as? AnyObject)?.value(forKey: "href") as? String ?? "")
                        
                        self.scenariosArray.append(scenarioObject)
                    }
                }
                callback(nil,"")
            }
            else
            {
                callback(Constants.errorMessage,"")
            }
        }
    }
    
    func getAScenarioForSelectedStrategy(strategyObject:Strategies, callback:@escaping (_ error:String?, _ resultStr:String?)->Void) {
        
        Alamofire.request(strategyObject.challengeLink, method: .get,  parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            let value = response.result.value as AnyObject
            
            if let completeMessage = value as? NSDictionary {
                
                let scenarioObject = Scenarios(
                    scenarioId: ((completeMessage.value(forKey: "_links") as? AnyObject)?.value(forKey: "challenge") as? AnyObject)?.value(forKey: "href") as? String ?? "",
                    title: completeMessage.value(forKey: "title") as? String ?? "",
                    desc: completeMessage.value(forKey: "description") as? String ?? "",
                    isFavourite: false,
                    numberOfStrategies: String(completeMessage.value(forKey: "strategies") as? Int ?? 0),
                    numberOfLikes: completeMessage.value(forKey: "thumbsUp") as? Int ?? 0,
                    strategiesArrayLink: ((completeMessage.value(forKey: "_links") as? AnyObject)?.value(forKey: "strategies") as? AnyObject)?.value(forKey: "href") as? String ?? "")
                
                strategyObject.challengeObject = scenarioObject
                callback(nil,"")
            }
            else
            {
                callback(Constants.errorMessage,"")
            }
        }
    }
    
    func addStrategy(challenge: String, title: String, description: String, patterns: [String], callback:@escaping (_ error:String?, _ resultStr:String?)->Void) {
        
        let params = ["challenge" : challenge, "title" : title, "description": description, "creator" : self.appUser.userID , "patterns": patterns] as [String : Any]
        
        Alamofire.request(Constants.baseURL + Constants.getAllStrategiesURL, method: .post,  parameters: params, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { (response) in
            
            let value = response.result.value as AnyObject
            
            if let valueIsDictionary = value as? NSDictionary{
                let StrategyObject = Strategies(
                    strategyID: ((valueIsDictionary.value(forKey: "_links") as? AnyObject)?.value(forKey: "strategy") as? AnyObject)?.value(forKey: "href") as? String ?? "",
                    title: valueIsDictionary.value(forKey: "title") as? String ?? "",
                    username: "",
                    desc: valueIsDictionary.value(forKey: "description") as? String ?? "",
                    isFavourite: false,
                    challengeLink: ((valueIsDictionary.value(forKey: "_links") as? AnyObject)?.value(forKey: "challenge") as? AnyObject)?.value(forKey: "href") as? String ?? "",
                    patternsURL: ((valueIsDictionary.value(forKey: "_links") as? AnyObject)?.value(forKey: "patterns") as? AnyObject)?.value(forKey: "href") as? String ?? "")
                
                self.strategiesArray.append(StrategyObject)
                
                callback(nil, "")
            }
            else{
                callback(Constants.errorMessage,"")
            }
        }
    }
    
    func addScenario(title: String, description: String, callback:@escaping (_ error:String?, _ resultStr:String?)->Void) {
        
        let params = ["isResolved" :"false", "title" : title, "description": description, "creator" : self.appUser.userID]
        
        Alamofire.request(Constants.baseURL + Constants.getAllScenariosURL, method: .post,  parameters: params, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { (response) in
            
            let value = response.result.value as AnyObject
            
            if let valueIsDictionary = value as? NSDictionary{
                let scenarioObject = Scenarios(
                    scenarioId:((valueIsDictionary.value(forKey: "_links") as? AnyObject)?.value(forKey: "challenge") as? AnyObject)?.value(forKey: "href") as? String ?? "",
                    title: valueIsDictionary.value(forKey: "title") as? String ?? "",
                    desc: valueIsDictionary.value(forKey: "description") as? String ?? "",
                    isFavourite: false,
                    numberOfStrategies: String(valueIsDictionary.value(forKey: "strategies") as? Int ?? 0),
                    numberOfLikes: valueIsDictionary.value(forKey: "thumbsUp") as? Int ?? 0,
                    strategiesArrayLink: ((valueIsDictionary.value(forKey: "_links") as? AnyObject)?.value(forKey: "strategies") as? AnyObject)?.value(forKey: "href") as? String ?? "")
                
                self.scenariosArray.append(scenarioObject)
                callback(nil,"")
            }
            else
            {
                callback(Constants.errorMessage,"")
            }
        }
    }
    
    func getAllStrategiesForScenario(scenarioObject:Patterns.Scenarios?, URLOfStrategy: String, callback:@escaping (_ error:String?, _ resultStr:String?)->Void) {
        
        Alamofire.request(URLOfStrategy, method: .get,  parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            let value = response.result.value as AnyObject
            let embeddedObject = value.value(forKey: "_embedded") as AnyObject
            let patternObjectArray = embeddedObject.value(forKey: "strategies") as AnyObject
            
            if let valuee = patternObjectArray as? NSArray {
                
                scenarioObject?.StrategiesArray.removeAll()
                
                for i in 0..<valuee.count
                {
                    if let completeMessage = valuee[i] as? NSDictionary
                    {
                        let StrategyObject = Strategies(
                            strategyID: ((completeMessage.value(forKey: "_links") as? AnyObject)?.value(forKey: "strategy") as? AnyObject)?.value(forKey: "href") as? String ?? "",
                            title: completeMessage.value(forKey: "title") as? String ?? "",
                            username: "",
                            desc: completeMessage.value(forKey: "description") as? String ?? "",
                            isFavourite: false,
                            challengeLink: "",
                            patternsURL: "")
                        
                        scenarioObject?.StrategiesArray.append(StrategyObject)
                    }
                }
                callback(nil,"")
            }
            else
            {
                callback(Constants.errorMessage,"")
            }
        }
    }
    
    func getAllPatterns(scenarioObject:Patterns.Scenarios?, callback:@escaping (_ error:String?, _ resultStr:String?)->Void) {
        
        Alamofire.request(Constants.baseURL + Constants.getAllStrategiesURL, method: .get,  parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            let value = response.result.value as AnyObject
            let embeddedObject = value.value(forKey:"_embedded") as AnyObject
            let patternObjectArray = embeddedObject.value(forKey:"patterns") as AnyObject
            if let valuee = patternObjectArray as? NSArray {
                
                self.patternsArray.removeAll()
                
                for i in 0..<valuee.count
                {
                    if let completeMessage = valuee[i] as? NSDictionary
                    {
                        let patternObject = PatternTab(
                            patternID: "",
                            title: completeMessage.value(forKey: "title") as? String ?? "",
                            desc: completeMessage.value(forKey: "description") as? String ?? "",
                            problem: completeMessage.value(forKey: "problem") as? String ?? "",
                            solution: completeMessage.value(forKey: "solution") as? String ?? "")
                        
                        self.patternsArray.append(patternObject)
                    }
                }
                callback(nil,"")
            }
            else
            {
                callback(Constants.errorMessage,"")
            }
        }
    }
    
    func getAllStrategies(scenarioObject:Patterns.Scenarios?, callback:@escaping (_ error:String?, _ resultStr:String?)->Void) {
        
        //        let params = ["token" : "9910e571d6be43548ae5300b40934b2d", "idScenario": scenarioObject.ID]
        
        Alamofire.request(Constants.baseURL + Constants.getAllStrategiesURL, method: .get,  parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            let value = response.result.value as AnyObject
            let embeddedObject = value.value(forKey:"_embedded") as AnyObject
            let strategyObject = embeddedObject.value(forKey:"strategies") as AnyObject
            if let valuee = strategyObject as? NSArray {
                
                self.strategiesArray.removeAll()
                
                for i in 0..<valuee.count
                {
                    if let completeMessage = valuee[i] as? NSDictionary
                    {
                        let StrategyObject = Strategies(
                            strategyID: ((completeMessage.value(forKey: "_links") as? AnyObject)?.value(forKey: "strategy") as? AnyObject)?.value(forKey: "href") as? String ?? "",
                            title: completeMessage.value(forKey: "title") as? String ?? "",
                            username: "",
                            desc: completeMessage.value(forKey: "description") as? String ?? "",
                            isFavourite: false,
                            challengeLink: ((completeMessage.value(forKey: "_links") as? AnyObject)?.value(forKey: "challenge") as? AnyObject)?.value(forKey: "href") as? String ?? "",
                            patternsURL: ((completeMessage.value(forKey: "_links") as? AnyObject)?.value(forKey: "patterns") as? AnyObject)?.value(forKey: "href") as? String ?? "")
                        
                        self.strategiesArray.append(StrategyObject)
                    }
                }
                callback(nil,"")
            }
            else
            {
                callback(Constants.errorMessage,"")
            }
        }
    }
    
    func getAllPatternsForStrategy(strategyObject:Patterns.Strategies?, callback:@escaping (_ error:String?, _ resultStr:String?)->Void) {
        
        Alamofire.request((strategyObject?.patternsURL)!, method: .get,  parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            let value = response.result.value as AnyObject
            let embeddedObject = value.value(forKey:"_embedded") as AnyObject
            let patternObjectArray = embeddedObject.value(forKey:"patterns") as AnyObject
            if let valuee = patternObjectArray as? NSArray {
                
                strategyObject?.patternsArray.removeAll()
                
                for i in 0..<valuee.count
                {
                    if let completeMessage = valuee[i] as? NSDictionary
                    {
                        let patternObject = PatternTab(
                            patternID: "",
                            title: completeMessage.value(forKey: "title") as? String ?? "",
                            desc: completeMessage.value(forKey: "description") as? String ?? "",
                            problem: completeMessage.value(forKey: "problem") as? String ?? "",
                            solution: completeMessage.value(forKey: "solution") as? String ?? "")
                        
                        //self.strategiesArray[0].patternsArray.append(patternObject)
                        strategyObject?.patternsArray.append(patternObject)
                    }
                }
                callback(nil,"")
            }
            else
            {
                callback(Constants.errorMessage,"")
            }
        }
    }
   
    
    func getPatterns(_ callback:@escaping (_ error:String?)->Void) {

        Alamofire.request(Constants.baseURL + Constants.getPatternsURL, method: .get,  parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (response) in

            let value = response.result.value as AnyObject
            let embeddedObject = value.value(forKey:"_embedded") as AnyObject
            let patternObjectArray = embeddedObject.value(forKey:"patterns") as AnyObject
            if let valuee = patternObjectArray as? NSArray {
                
                self.patternsArray.removeAll()
                
                for i in 0..<valuee.count
                {
                    if let completeMessage = valuee[i] as? NSDictionary
                    {
                        let patternObject = PatternTab(
                            patternID: ((completeMessage.value(forKey: "_links") as? AnyObject)?.value(forKey: "pattern") as? AnyObject)?.value(forKey: "href") as? String ?? "",
                            title: completeMessage.value(forKey: "title") as? String ?? "",
                            desc: completeMessage.value(forKey: "description") as? String ?? "",
                            problem: completeMessage.value(forKey: "problem") as? String ?? "",
                            solution: completeMessage.value(forKey: "solution") as? String ?? "")
                        
                        self.patternsArray.append(patternObject)
                    }
                }
                callback(nil)
            }
            else
            {
                callback(Constants.errorMessage)
            }
        }
    }

    // MARK:- User Sign In
    
    func signInWithEmail(_ username:String, password:String, callback:@escaping (_ error:String?)->Void) {
        
        let params = ["username":username, "password":password]
        
        Alamofire.request("http://advvs25.gm.fh-koeln.de/users", method: .post,  parameters: params, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { (response) in
            
            let value = response.result.value as AnyObject
            let linksObject = value.value(forKey:"_links") as AnyObject
            if let selfObject = (linksObject.value(forKey:"self") as AnyObject).value(forKey: "href") {
                
                print(selfObject)
                
                let appUser = AppUser(username: username, password: password, userID: selfObject as! String)
                self.appUser = appUser
                self.saveAppUserInUserDefaults(appUser)
                
                callback(nil)
            }
            else
            {
                callback(Constants.errorMessage)
            }
        }
    }
    
    func signInWithUserDefaultsIfPossible(_ callback:(_ appUser:AppUser?) -> Void) {
        
        self.appUser = self.getAppUserFromUserDefaults()
        callback(appUser)
    }
    
    // MARK:- Logout 
    
    func logout() {
        
        self.removeAppUserFromUserDefaults()
    }


    func savePatternsArrayInUserDefaults() {
        
        NSKeyedArchiver.setClassName(Constants.patternsArrayKey, for: PatternTab.self)
        let data = NSKeyedArchiver.archivedData(withRootObject: self.patternsArray)
        let userDefaults = UserDefaults.standard
        userDefaults.set(data, forKey: Constants.patternsArrayKey)
        userDefaults.synchronize()
    }
    
    func getPatternsArrayFromUserDefaults() -> [PatternTab]?
    {
        let userDefaults = UserDefaults.standard
        let data = userDefaults.data(forKey: Constants.patternsArrayKey)
        var tempPatternsArray:[PatternTab]?
        
        if data != nil {
            
            NSKeyedUnarchiver.setClass(PatternTab.self, forClassName: Constants.patternsArrayKey)
            tempPatternsArray = NSKeyedUnarchiver.unarchiveObject(with: data!) as? [PatternTab]
        }
        
        return tempPatternsArray
    }
    
    func saveChallengesArrayInUserDefaults() {
        
        NSKeyedArchiver.setClassName(Constants.challengesArrayKey, for: Scenarios.self)
        let data = NSKeyedArchiver.archivedData(withRootObject: self.scenariosArray)
        let userDefaults = UserDefaults.standard
        userDefaults.set(data, forKey: Constants.challengesArrayKey)
        userDefaults.synchronize()
    }
    
    func getChallengesArrayFromUserDefaults() -> [Scenarios]?
    {
        let userDefaults = UserDefaults.standard
        let data = userDefaults.data(forKey: Constants.challengesArrayKey)
        var tempChallengesArray:[Scenarios]?
        
        if data != nil {
            
            NSKeyedUnarchiver.setClass(Scenarios.self, forClassName: Constants.challengesArrayKey)
            tempChallengesArray = NSKeyedUnarchiver.unarchiveObject(with: data!) as? [Scenarios]
        }
        
        return tempChallengesArray
    }
    
    func saveStrategiesArrayInUserDefaults() {
        
        NSKeyedArchiver.setClassName(Constants.strategiesArrayKey, for: Strategies.self)
        let data = NSKeyedArchiver.archivedData(withRootObject: self.strategiesArray)
        let userDefaults = UserDefaults.standard
        userDefaults.set(data, forKey: Constants.strategiesArrayKey)
        userDefaults.synchronize()
    }
    
    func getStrategiesArrayFromUserDefaults() -> [Strategies]?
    {
        let userDefaults = UserDefaults.standard
        let data = userDefaults.data(forKey: Constants.strategiesArrayKey)
        var tempStrategiesArray:[Strategies]?
        
        if data != nil {
            
            NSKeyedUnarchiver.setClass(Strategies.self, forClassName: Constants.strategiesArrayKey)
            tempStrategiesArray = NSKeyedUnarchiver.unarchiveObject(with: data!) as? [Strategies]
        }
        
        return tempStrategiesArray
    }
    
    // MARK:- Helper Private Functions
    func saveAppUserInUserDefaults(_ appUser:AppUser) {
        
        NSKeyedArchiver.setClassName(Constants.appUser, for: AppUser.self)
        let data = NSKeyedArchiver.archivedData(withRootObject: appUser)
        let userDefaults = UserDefaults.standard
        userDefaults.set(data, forKey: Constants.appUser)
    }
    
    fileprivate func getAppUserFromUserDefaults()->AppUser?
    {
        let userDefaults = UserDefaults.standard
        let data = userDefaults.data(forKey: Constants.appUser)
        var appUser:AppUser?
        
        if data != nil {
            
            NSKeyedUnarchiver.setClass(AppUser.self, forClassName: "AppUser")
            appUser = NSKeyedUnarchiver.unarchiveObject(with: data!) as? AppUser
        }
        
        return appUser
    }
    
    fileprivate func removeAppUserFromUserDefaults() {
        
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: Constants.appUser)
        userDefaults.synchronize()
    }
    
    private func checkIfTokenIsExpiredAndTriggerDelegateFor(response:DataResponse<Any>, delegate:PatternsDelegate) -> Bool {
        
        if response.response?.statusCode == 401 {
            
            delegate.PatternsAppUserTokenIsExpired()
            return true
        }
        
        return false
    }
    
    
    // MARK:- Classes
    @objc(AppUser)
    class AppUser : NSObject, NSCoding {
        
        var username : String
        var password : String
        var userID: String
        
        init(username : String, password : String, userID: String) {
        
            self.username = username
            self.password = password
            self.userID = userID
        }
        
        required init?(coder aDecoder: NSCoder) {
            
            self.username = aDecoder.decodeObject(forKey: Constants.usernameKey) as? String ?? ""
            self.password = aDecoder.decodeObject(forKey: Constants.passwordKey) as? String ?? ""
            self.userID = aDecoder.decodeObject(forKey: Constants.userIdKey) as? String ?? ""
        }
        
        func encode(with aCoder: NSCoder) {
            
            aCoder.encode(self.username, forKey: Constants.usernameKey)
            aCoder.encode(self.password, forKey: Constants.passwordKey)
            aCoder.encode(self.userID, forKey: Constants.userIdKey)
        }
    }
    
    @objc(Scenarios)
    class Scenarios: NSObject, NSCoding {
        
        var scenarioId: String
        var title: String
        var desc: String
        var isFavourite: Bool
        var numberOfStrategies: String
        var numberOfLikes: Int
        var strategiesArrayLink: String
        var StrategiesArray = [Strategies]()
        
        init(scenarioId: String, title:String, desc: String, isFavourite: Bool, numberOfStrategies: String, numberOfLikes: Int, strategiesArrayLink: String) {
            
            self.scenarioId = scenarioId
            self.title = title
            self.desc = desc
            self.isFavourite = isFavourite
            self.numberOfStrategies = numberOfStrategies
            self.numberOfLikes = numberOfLikes
            self.strategiesArrayLink = strategiesArrayLink
        }
        
        required init?(coder aDecoder: NSCoder) {
            
            self.scenarioId = aDecoder.decodeObject(forKey: "scenarioId") as? String ?? ""
            self.title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
            self.desc = aDecoder.decodeObject(forKey: "desc") as? String ?? ""
            
            self.isFavourite = aDecoder.decodeBool(forKey: "isFavourite")
            self.numberOfStrategies = aDecoder.decodeObject(forKey: "numberOfStrategies") as? String ?? ""
            self.numberOfLikes = aDecoder.decodeObject(forKey: "numberOfLikes") as? Int ?? 0
            self.strategiesArrayLink = aDecoder.decodeObject(forKey: "strategiesArrayLink") as? String ?? ""
            self.StrategiesArray = aDecoder.decodeObject(forKey: "StrategiesArray") as? [Strategies] ?? []
        }
        
        func encode(with aCoder: NSCoder) {
            
            aCoder.encode(self.scenarioId, forKey: "scenarioId")
            aCoder.encode(self.title, forKey: "title")
            aCoder.encode(self.desc, forKey: "desc")
            aCoder.encode(self.isFavourite, forKey: "isFavourite")
            aCoder.encode(self.numberOfStrategies, forKey: "numberOfStrategies")
            aCoder.encode(self.numberOfLikes, forKey: "numberOfLikes")
            aCoder.encode(self.strategiesArrayLink, forKey: "strategiesArrayLink")
            aCoder.encode(self.StrategiesArray, forKey: "StrategiesArray")
        }
    }
    
    @objc(Strategies)
    class Strategies: NSObject, NSCoding {
        
        var strategyID: String
        var title: String
        var username: String
        var desc: String
        var isFavourite: Bool
        var patternsURL: String
        var challengeLink: String
        var challengeObject: Scenarios!
        var patternsArray = [PatternTab]()
        
        init(strategyID: String, title: String, username: String, desc: String, isFavourite: Bool, challengeLink: String, patternsURL: String)
        {
            self.strategyID = strategyID
            self.title = title
            self.username = username
            self.desc = desc
            self.isFavourite = isFavourite
            self.challengeLink = challengeLink
            self.patternsURL = patternsURL
        }
        
        required init?(coder aDecoder: NSCoder) {
            
            self.strategyID = aDecoder.decodeObject(forKey: "strategyID") as? String ?? ""
            self.title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
            self.desc = aDecoder.decodeObject(forKey: "desc") as? String ?? ""
            self.username = aDecoder.decodeObject(forKey: "username") as? String ?? ""
            self.isFavourite = aDecoder.decodeBool(forKey: "isFavourite")
            self.patternsURL = aDecoder.decodeObject(forKey: "patternsURL") as? String ?? ""
            self.challengeLink = aDecoder.decodeObject(forKey: "challengeLink") as? String ?? ""
            self.challengeObject = aDecoder.decodeObject(forKey: "challengeObject") as? Scenarios ?? nil
            self.patternsArray = aDecoder.decodeObject(forKey: "patternsArray") as? [PatternTab] ?? []
        }
        
        func encode(with aCoder: NSCoder) {
            
            aCoder.encode(self.strategyID, forKey: "strategyID")
            aCoder.encode(self.title, forKey: "title")
            aCoder.encode(self.desc, forKey: "desc")
            aCoder.encode(self.username, forKey: "username")
            aCoder.encode(self.isFavourite, forKey: "isFavourite")
            aCoder.encode(self.patternsURL, forKey: "patternsURL")
            aCoder.encode(self.challengeLink, forKey: "challengeLink")
            aCoder.encode(self.challengeObject, forKey: "challengeObject")
            aCoder.encode(self.patternsArray, forKey: "patternsArray")
        }
    }
    
    @objc(PatternTab)
    class PatternTab : NSObject, NSCoding{
        
        var patternID: String
        var title: String
        var desc : String
        var problem: String
        var solution: String
        
        var tagsArray = [String]()
        var notesArray = [Notes]()
        
        init(patternID: String, title: String, desc : String, problem: String, solution: String)
        {
            self.patternID = patternID
            self.title = title
            self.desc = desc
            self.problem = problem
            self.solution = solution
        }
        
        required init?(coder aDecoder: NSCoder) {
            self.patternID = aDecoder.decodeObject(forKey: "patternID") as? String ?? ""
            self.title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
            self.desc = aDecoder.decodeObject(forKey: "desc") as? String ?? ""
            self.problem = aDecoder.decodeObject(forKey: "problem") as? String ?? ""
            self.solution = aDecoder.decodeObject(forKey: "solution") as? String ?? ""
            self.tagsArray = aDecoder.decodeObject(forKey: "tagsArray") as? [String] ?? []
            self.notesArray = aDecoder.decodeObject(forKey: "notesArray") as? [Notes] ?? []
        }
        
        func encode(with aCoder: NSCoder) {
            
            aCoder.encode(self.patternID, forKey: "patternID")
            aCoder.encode(self.title, forKey: "title")
            aCoder.encode(self.desc, forKey: "desc")
            aCoder.encode(self.problem, forKey: "problem")
            aCoder.encode(self.solution, forKey: "solution")
            aCoder.encode(self.tagsArray, forKey: "tagsArray")
            aCoder.encode(self.notesArray, forKey: "notesArray")
        }
    }
    
    @objc(Notes)
    class Notes : NSObject, NSCoding{
        
        var noteTitle: String
        var noteText: String
        var noteDate: String
        
        init(noteTitle: String, noteText: String, noteDate: String) {
            self.noteTitle = noteTitle
            self.noteText = noteText
            self.noteDate = noteDate
        }
        
        required init?(coder aDecoder: NSCoder) {
            self.noteTitle = aDecoder.decodeObject(forKey: "noteTitle") as? String ?? ""
            self.noteText = aDecoder.decodeObject(forKey: "noteText") as? String ?? ""
            self.noteDate = aDecoder.decodeObject(forKey: "noteDate") as? String ?? ""
        }
        
        func encode(with aCoder: NSCoder) {
            
            aCoder.encode(self.noteTitle, forKey: "noteTitle")
            aCoder.encode(self.noteText, forKey: "noteText")
            aCoder.encode(self.noteDate, forKey: "noteDate")
            
        }
    }

    
    class Constants {
        
        // Service
        
        static let baseURL = "http://advvs25.gm.fh-koeln.de/"
        static let getAllScenariosURL = "challenges"
        static let getAllStrategiesURL = "strategies"
        static let addScenarioURL = "users/scenarios"
        static let getPatternsURL = "patterns"
        
        static let errorMessage = "Something went wrong"
        static let fillAllFieldsMessage = "Please fill the required data"
        
        static let missingFieldsError = "Please fill all the fields"
        static let validEmailError = "Please enter a valid email"
        static let incorrectCredentials = "The credentials provided are incorrect."
        
        static let navigationBarColor = UIColor.init(red: 32/255.0, green: 84/255.0, blue: 192/255.0, alpha: 1.0)
        
        static let result = "result"
        static let usernameKey = "username"
        static let passwordKey = "password"
        static let userIdKey = "userId"
        
        static let deviceRegistered = "deviceRegistered"
        static let username = "username"
        static let message = "message"
        

        // For user defaults
        static let appUser = "AppUser"
        static let patternsArrayKey = "patternsArrayKey"
        static let challengesArrayKey = "challengesArrayKey"
        static let strategiesArrayKey = "strategiesArrayKey"
        static let notesArrayKey = "notesArrayKey"
        
    }

}

protocol PatternsDelegate {
    
    func PatternsAppUserTokenIsExpired()
}

