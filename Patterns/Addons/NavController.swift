//
//  NavController.swift
//  StudentApp
//
//  Created by Macbook on 03/03/2018.
//  Copyright © 2018 Macbook. All rights reserved.
//

import UIKit

class NavController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //[UIColor colorWithRed:(32/255.0) green:(84/255.0) blue:(192/255.0) alpha:1.0];
        
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationBar.titleTextAttributes = textAttributes
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.barTintColor = Patterns.Constants.navigationBarColor
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
