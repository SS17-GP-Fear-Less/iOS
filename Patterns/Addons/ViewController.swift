//
//  ViewController.swift
//  illustration
//
//  Created by Admin on 8/1/16.
//  Copyright © 2016 Centegy Technologies. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    
    @IBOutlet weak var homeContainerView: UIView!
    @IBOutlet weak var menuBarButton: UIBarButtonItem!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil
        {
            menuBarButton.target = self.revealViewController()
            menuBarButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            revealViewController().rearViewRevealWidth = self.view.frame.size.width/1.5
        }
    }
    
    func revealController(revealController: SWRevealViewController!, didMoveToPosition position: FrontViewPosition) {
        
        if position.hashValue == 4 {
            
            self.homeContainerView.isUserInteractionEnabled = false
            revealViewController().tapGestureRecognizer().isEnabled = true
            
        }
        else
        {
            self.homeContainerView.isUserInteractionEnabled = true
            revealViewController().tapGestureRecognizer().isEnabled = false
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

