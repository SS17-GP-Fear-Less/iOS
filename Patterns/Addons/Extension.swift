//
//  Extension.swift
//  Hit With Me
//
//  Created by Plutotek on 29/10/2016.
//  Copyright © 2016 Plutotek. All rights reserved.
//

import Foundation
import UIKit
import Contacts

extension UIViewController  {
    
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Jump To ProfileScreen
//    func jumpToProfileOfUser(user:Patterns.User, viewController:UIViewController? = nil) {
//
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let profileVC = storyBoard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileViewController
//        profileVC.user = user
//
//        if let _viewCOntroller = viewController {
//            _viewCOntroller.navigationController?.pushViewController(profileVC, animated: true)
//        }
//        else {
//            self.navigationController?.pushViewController(profileVC, animated: true)
//        }
//    }
    
}


extension UIImage {
    
    public static func defaultUserImage() -> UIImage {
        
        return UIImage(named: "default_user_icon")!
        
    }
    
    func resizeImage(newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height: newHeight))
        self.draw(in: CGRect(x:0, y:0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}


extension Date {
    
    public func getDateAsStringOfPatternsFormat() -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
        
        return formatter.string(from: self)
        
    }
    
    
    public func getAsHumanReadable() -> String {
        
        func removeAboutStringAndAddAgoAtTheEnd(str:String) -> String {
            
            let range = str.range(of:"About ")
            
            let stringWithoutAbout = str.substring(from: range!.upperBound)
            
            let stringWithAgo = stringWithoutAbout + " ago"
            
            return stringWithAgo
            
        }
        
        func getFirstComponentIfBreakableByMeansOfComma(str:String) -> String {
            
            let timeDifferenceComponents = str.characters.split{$0 == ","}.map(String.init)
            
            let firstTimeDifferenceComponents:String? = timeDifferenceComponents.count > 0 ? timeDifferenceComponents[0] : nil
            
            return firstTimeDifferenceComponents ?? str
            
        }
        
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = DateComponentsFormatter.UnitsStyle.full
        formatter.includesApproximationPhrase = true
        formatter.includesTimeRemainingPhrase = false
        formatter.allowedUnits = [.year, .month, .weekOfMonth, .day, .hour, .minute, .second]
        formatter.maximumUnitCount = 1
        
        let dateRelativeString = formatter.string(from: self, to: Date())
        
        let stringFirstComponent = getFirstComponentIfBreakableByMeansOfComma(str: dateRelativeString!)
        
        let stringWithAgoAtTheEnd = removeAboutStringAndAddAgoAtTheEnd(str: stringFirstComponent)
        
        return stringWithAgoAtTheEnd
    }
}


extension String {
    
    public func getAsDate() -> Date? {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"

        return formatter.date(from: self)
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    var encodeEmoji: String? {
        let encodedStr = NSString(cString: self.cString(using: String.Encoding.nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue)
        return encodedStr as? String
    }
    
    var decodeEmoji: String {
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        if data != nil {
            let valueUniCode = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue) as? String
            if valueUniCode != nil {
                return valueUniCode!
            } else {
                return self
            }
        } else {
            return self
        }
    }
}


extension UIButton {
    
    
    public func pulseAnimationOn() {
        
        let pulseAnimation = CABasicAnimation(keyPath: "opacity")
        pulseAnimation.duration = 0.2
        pulseAnimation.fromValue = 0.5
        pulseAnimation.toValue = 1
        pulseAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        pulseAnimation.autoreverses = true
        pulseAnimation.repeatCount = Float.greatestFiniteMagnitude
        self.layer.add(pulseAnimation, forKey: "animateOpacity")
    }
    
    public func pulseAnimationOff() {
        
        self.layer.removeAnimation(forKey: "animateOpacity")
        
    }
    
}


extension UIColor {
    
    public class func PatternsStatusBarColor() -> UIColor {
        return UIColor(red: 72/255, green: 168/255, blue: 224/255, alpha: 1.0)
    }
    
    public class func PatternsLightBlueColor() -> UIColor {
     
        return UIColor(red: 38/255, green: 153/255, blue: 218/255, alpha: 1.0)

    }
    
    public class func PatternsBlueColor() -> UIColor {
        
        return UIColor(red: 41/255, green: 152/255, blue: 217/255, alpha: 1.0)
        
    }
    
    public class func PatternsLightGreenColor() -> UIColor {
        
        return UIColor(red: 130/255, green: 183/255, blue: 111/255, alpha: 1.0)
        
    }
    
    public class func PatternsVeryLightGreenColor() -> UIColor {
        
        return UIColor(red: 234/255, green: 247/255, blue: 233/255, alpha: 1.0)
    }
    
    public class func PatternsGreenColor() -> UIColor {
        
        return UIColor(red: 36/255, green: 156/255, blue: 0/255, alpha: 1.0)
    }
    
    public class func PatternsDarkYellowColor() -> UIColor {
        
        return UIColor(red: 211/255, green: 187/255, blue: 0/255, alpha: 1.0)
        
    }
    
    public class func hitWithVeryLightBlueColor() -> UIColor {
        
        return UIColor(red: 177/255, green: 223/255, blue: 243/255, alpha: 1.0)
    }
}


extension UISegmentedControl {
    
    public func setContentModeOfSubViews(_ contentMode:UIViewContentMode) {
        
        for view in self.subviews {
            
            if let imageView = view as? UIImageView {
                
                imageView.contentMode = .scaleAspectFit
            }
        }
    }
    
    func makeMultiline(numberOfLines: Int)
    {
        for segment in self.subviews
        {
            let labels = segment.subviews.filter { $0 is UILabel }	// [AnyObject]
            
            for label in labels {
                
                if let _label = label as? UILabel {
                    _label.font = _label.font.withSize(_label.font.pointSize - 5)
                    _label.numberOfLines = numberOfLines
                }
            }
            
        }
    }
    
    
    
}


extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        
        
        get {
            
            if layer.borderColor == nil {
                return UIColor.clear
            }
            else {
                return UIColor(cgColor: layer.borderColor!)
            }
        }
        set {
            
            layer.borderColor = newValue.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        
        get {
            return layer.borderWidth
        }
        set {
            
            if newValue < 0 {
                layer.borderWidth = 0
            }
            else {
                layer.borderWidth = newValue
                clipsToBounds = true
            }
        }
    }
    
    @IBInspectable var ShadowRadius: CGFloat {
        
        get {
            return layer.shadowRadius
        }
        set {
            
            if newValue < 0 {
                layer.shadowRadius = 0
            }
            else {
                layer.masksToBounds = false
                
                layer.shadowOpacity = 0.5
                layer.shadowOffset = CGSize(width: -1, height: 1)
                
                layer.shadowPath = UIBezierPath(rect: bounds).cgPath
                layer.shouldRasterize = true
                
                layer.shadowRadius = newValue
            }
        }
    }
    
    @IBInspectable var ShadowColor: UIColor {
        get {
            
            if layer.shadowColor == nil {
                return UIColor.clear
            }
            else {
                return UIColor(cgColor: layer.shadowColor!)
            }
        }
        set {
            
            layer.shadowColor = newValue.cgColor
        }
    }
    
}

